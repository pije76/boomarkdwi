# Bookmark link useful Untuk grup DWI #

## Link Tutorial berbayar (premium) : ##

- Tutsplus : [tutsplus.com ](http://tutsplus.com)
- Lynda : [Lynda.com](http://lynda.com)
- Teamtreehouse : [http://teamtreehouse.com](http://teamtreehouse.com)

## Link Tutorial gratis dan blog ttg web designer: ##
	
- Webdesign tutsplus : [http://webdesign.tutsplus.com](http://webdesign.tutsplus.com)
- Codrops : [http://tympanus.net/codrops](http://tympanus.net/codrops)
- Smashing Magazine : [http://smashingmagazine.com](http://smashingmagazine.com)
- Css triks : [http://css-tricks.com](http://css-tricks.com)
- 1stwebdesigner : [http://1stwebdesigner.com](http://1stwebdesigner.com)
- webdesignerdepot : [http://webdesignerdepot.com](http://webdesignerdepot.com)

## Specific tutorial and for beginer ##

**PSD to HTML**

- Webdesigntuts+ From Photoshop to Wordpress : [link](http://webdesign.tutsplus.com/sessions/adaptive-blog-theme-from-photoshop-to-wordpress/)   *bagian lanjutan wordpressnya ada di wp.tutsplus.com
- Design a Warm, Cheerful Website Interface in Adobe Photoshop : [Link](http://webdesign.tutsplus.com/tutorials/design-a-warm-cheerful-website-interface-in-adobe-photoshop/)  
- Convert a Warm, Cheerful Web Design to HTML and CSS : [link](http://net.tutsplus.com/tutorials/site-builds/convert-a-warm-cheerful-web-design-to-html-and-css/)
- Create a Sleek, Corporate Web Design :
	- [Day 1](http://webdesign.tutsplus.com/tutorials/create-a-sleek-corporate-web-design-part-13/)
	- [Day 2](http://webdesign.tutsplus.com/tutorials/complete-websites/create-a-sleek-corporate-web-design-hd-video-series-day-2/)
	- [Day 3](http://webdesign.tutsplus.com/tutorials/complete-websites/create-a-sleek-corporate-web-design-hd-video-series-day-3/)
	- [Day 4](http://webdesign.tutsplus.com/tutorials/complete-websites/create-a-sleek-corporate-web-design-hd-video-series-day-4/) 

## Tentang UI dan UX ##

- UX : [http://uxapprentice.com](http://uxapprentice.com)
- UI : [http://goodui.org](http://goodui.org)


**Ebook**

- Ebook Belajar HTML & CSS by ariona : [link](http://www.ariona.net/ebook-belajar-html-dan-css/)

## Resources

- Freebies from blog : 
	- Webdesignerdepot 

------------

- Free Image : 
	- Unsplash : [unsplash.com](http://unsplash.com) Free Image HD per 10 Days
	- Compfight : [http://compfight.com](http://compfight.com "Compfight") 
	- Graphicriver : [http://graphicriver.net](http://graphicriver.net) note : ini cuma sebulan sekali gratisannnya.

----------

- Free Icon : 
	- Gemicon  : [gemicon.net](gemicon.net)
	- Entypo : [http://www.entypo.com/](http://www.entypo.com/)

----------

- Free UI :
	- Designmodo : [Flat Free UI](http://designmodo.com/flat-free/)  
	- Free Button UI : [http://bit.ly/11etR68](http://bit.ly/11etR68)

sabar ya tiap hari ditambah teruuuus~